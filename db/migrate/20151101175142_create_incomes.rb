class CreateIncomes < ActiveRecord::Migration
  def change
    create_table :incomes do |t|
      t.integer :amount
      t.integer :category_id
      t.date :earned_on

      t.timestamps null: false
    end
  end
end
