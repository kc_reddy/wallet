class CostsController < ApplicationController
  before_action :set_cost, only: [:show, :edit, :update, :destroy]

  # GET /costs
  # GET /costs.json
  def index
   @costs = Cost.all
   @costs_order = Cost.order(spent_on: :asc)
   #@costs_sum = Cost.select("category_id, sum(amount) as total_amount").group("category_id")
  #@costs_sum = Cost.sum(:amount)
    @costs_sum = Cost.group(:category_id).sum(:amount)
    date1 = Date.parse('august 1 2015')
    date2 = Date.parse('august 10 2015')
    @cost_daterange = Cost.where(:spent_on => date1..date2)
    @costs_sum_daterange = @cost_daterange.group(:category_id).sum(:amount)
    @costs_total = Cost.sum(:amount)
    if @costs_total >= (0.9) * Income.sum(:amount) 
      flash.now[:alert] = "Spent 90% of your income"
      flash[:alert] = "Spent 90% of your income"
    end
    @incomes_total = Income.sum(:amount)
   @costs_sum_daterange_join = Cost.joins(:category).order(spent_on: :asc)

   #@costs_sum_daterange_join = Cost.all(:all, :joins => :category)

  end

  # GET /costs/1
  # GET /costs/1.json
  def show
  end

  # GET /costs/new
  def new
    @cost = Cost.new
  end

  def there
     
   end

  # GET /costs/1/edit
  def edit
  end

  def range

  end

  def order
  date1 = params[:date1] 
  date2 = params[:date2]
    #@date1 = Date.parse('august 1 2015')
    #@date2 = Date.parse('august 10 2015')
    #@costs_daterange = Cost.find(:all, :conditions => ['spent_on >= ? and spent_on <= ?', params[:start_date], params[:end_date]])
   @cost_daterange = Cost.all.where(:spent_on => date1..date2)
   @costs_sum_daterange = @cost_daterange.group(:category_id).sum(:amount)

   respond_to do |format|
  #format.html { redirect_to @cost, notice: date2 }       
    format.html { render :order, notice: 'Cost was successfully created.'}
             end
  end

  # POST /costs
  # POST /costs.json
  def create
    @cost = Cost.new(cost_params)
    respond_to do |format|
      if @cost.save
        @costs_total = Cost.sum(:amount)
        if @costs_total >= (0.9) * Income.sum(:amount)
          flash[:alert] = "Spent 90% of your income"
          flash.now[:alert] = "Spent 90% of your income"
        end
      #date1 = params[:date1] 
        format.html { redirect_to @cost , alert: "ALERT!!  Spent 90% of your income", notice: 'Cost was successfully created.'  }
        format.json { render :show, status: :created, location: @cost }
      else
        format.html { render :new }
        format.json { render json: @cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /costs/1
  # PATCH/PUT /costs/1.json
  def update
    respond_to do |format|
      if @cost.update(cost_params)
        format.html { redirect_to @cost, notice: 'Cost was successfully updated.' }
        format.json { render :show, status: :ok, location: @cost }
      else
        format.html { render :edit }
        format.json { render json: @cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /costs/1
  # DELETE /costs/1.json
  def destroy
    @cost.destroy
    respond_to do |format|
      format.html { redirect_to costs_url, notice: 'Cost was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

skip_before_filter :verify_authenticity_token
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cost
      @cost = Cost.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cost_params
      params.require(:cost).permit(:amount, :category_id, :spent_on)
    end
end
