class Cost < ActiveRecord::Base
	belongs_to :category
	validates :amount, presence: true
	validates :category_id, presence: true
	validates :spent_on, presence: true	
end
