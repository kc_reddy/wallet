class Income < ActiveRecord::Base
	belongs_to :category
	validates :amount, presence: true
	validates :category_id, presence: true
	validates :earned_on, presence: true
end
