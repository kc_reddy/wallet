json.array!(@incomes) do |income|
  json.extract! income, :id, :amount, :category_id, :earned_on
  json.url income_url(income, format: :json)
end
