json.array!(@costs) do |cost|
  json.extract! cost, :id, :amount, :category_id, :spent_on
  json.url cost_url(cost, format: :json)
end
