require 'test_helper'

class CostTest < ActiveSupport::TestCase
  test "invalid_with_empty_attributes" do
  	cost = Cost.new
  	assert cost.invalid?
  	assert cost.errors[:category_id].any?
  	assert cost.errors[:amount].any? 
  	assert cost.errors[:spent_on].any? 	
  end

  test "requires_amount_&_spent_on" do
  	cost = Cost.create(:category_id => 8)
  	assert cost.invalid?
  	assert cost.errors[:spent_on].any?
  	assert cost.errors[:amount].any?
  	end

  test "valid_with_category_id_&_amount_&_spent_on" do
    cost = Cost.create(:category_id => 8, :amount => 10, :spent_on => 'Sat, 29 Aug 2015')
    assert cost.valid?
  end



  end
