Rails.application.routes.draw do
  filter :locale

  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/ do
    resources :costs do
      collection do
        post 'order'
        get 'range'             
      end

    get '*path', to: redirect("/#{I18n.default_locale}/%{path}")
    #get '', to: redirect("/#{I18n.default_locale}")
    post '*path', to: redirect("/#{I18n.default_locale}/%{path}")
    #post '', to: redirect("/#{I18n.default_locale}")
    end

    resources :incomes
    resources :categories

end

# get '*path', to: redirect("/#{I18n.locale}/%{path}")
# #get '', to: redirect("/#{I18n.locale}")
# post '*path', to: redirect("/#{I18n.locale}/%{path}")
#post '', to: redirect("/#{I18n.locale}")

#  get "*path", to: "locale#not_found"

  # get 'costs/order' => 'costs_controller#order'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   root 'costs#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
